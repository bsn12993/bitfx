﻿using BITFX.Connection;
using BITFX.Interface;
using BITFX.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BITFX.DAO
{
    public class EstatusDAO : IBase
    {
        /// <summary>
        /// Metodo que obtiene el listado de estatus existentes
        /// </summary>
        /// <returns></returns>
        public Response FindAll()
        {
            List<Estatus> lstEstatus = null;
            Estatus Estatus = null;
            try
            {
                lstEstatus = new List<Estatus>();

                var connection = new BDConnection().getConnection();
                if (!connection.IsSuccess)
                {
                    return connection;
                }

                using (var sql = new SqlConnection(connection.Result.ToString()))
                {
                    using (var cmd = new SqlCommand(@"SELECT id_estatus
                                          ,descripcion
                                      FROM Cat_Estatus order by descripcion asc", sql))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        sql.Open();
                        var rd = cmd.ExecuteReader();
                        if (rd.HasRows)
                        {
                             
                            while (rd.Read())
                            {
                                Estatus = new Estatus();
                                Estatus.Estatus_ID = (int)rd["id_estatus"];
                                Estatus.Descripcion = (string)rd["descripcion"];
                                lstEstatus.Add(Estatus);
                            }

                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se encontraron datos",
                                Result = lstEstatus
                            };
                        }
                        else
                            throw new Exception("No se encontraron datos");
                    }
                }
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }
    }
}