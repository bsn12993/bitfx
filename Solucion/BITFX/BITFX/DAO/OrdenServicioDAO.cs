﻿using BITFX.Connection;
using BITFX.Interface;
using BITFX.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace BITFX.DAO
{
    public class OrdenServicioDAO : IOrdenServicio
    {
        /// <summary>
        /// Metodo que obtiene todos los registros existentes de la tabla
        /// </summary>
        /// <returns></returns>
        public Response FindAll()
        {
            List<OrdenServicio> lstOrdenServicio = null;
            OrdenServicio ordenServicio = null;
            try
            {
                var connection = new BDConnection().getConnection();
                if (!connection.IsSuccess)
                {
                    return connection;
                }

                using (var sql = new SqlConnection(connection.Result.ToString()))
                {
                    using (var cmd = new SqlCommand(@"select 
                                os.id_ordenservicio,
                                os.num_servicio,
                                cli.id_cliente,
                                cli.descripcion,
                                os.tipo_servicio,
                                os.submarca,
                                os.precio,
                                os.fecha,
                                os.id_estatus,
                                est.descripcion estatus
                                from OrdenServicio os 
                                inner join Cat_Cliente cli on os.id_cliente=cli.id_cliente
                                inner join Cat_Estatus est on os.id_estatus=est.id_estatus order by num_servicio asc", sql))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        sql.Open();
                        var rd = cmd.ExecuteReader();
                        if (rd.HasRows)
                        {
                            lstOrdenServicio = new List<OrdenServicio>();
                            while (rd.Read())
                            {
                                ordenServicio = new OrdenServicio();
                                ordenServicio.Servicio_ID = (int)rd["id_ordenservicio"];
                                ordenServicio.NumServicio = (int)rd["num_servicio"];
                                ordenServicio.Cliente.Cliente_ID = (int)rd["id_cliente"];
                                ordenServicio.Cliente.Nombre = (string)rd["descripcion"];
                                ordenServicio.Precio = (decimal)rd["precio"];
                                ordenServicio.Fecha = (DateTime)rd["fecha"];
                                ordenServicio.Estatus.Estatus_ID = (int)rd["id_estatus"];
                                ordenServicio.Estatus.Descripcion = (string)rd["estatus"];
                                ordenServicio.SubMarca = (string)rd["submarca"];
                                ordenServicio.TipoServicio = (string)rd["tipo_servicio"];
                                lstOrdenServicio.Add(ordenServicio);
                            }

                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se encontraron datos",
                                Result = lstOrdenServicio
                            };
                        }
                        else
                            throw new Exception("No se encontraron datos");
                    }
                }
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// Método que realiza una búsqueda en base a parametros, estatus y rango de fechas
        /// </summary>
        /// <param name="estatus">estatus</param>
        /// <param name="fechaInicio">fecha inicio</param>
        /// <param name="fechaFin">fecha fin</param>
        /// <returns></returns>
        public Response FindByFilter(int estatus, DateTime? fechaInicio, DateTime? fechaFin)
        {
            List<OrdenServicio> lstOrdenServicio = null;
            OrdenServicio ordenServicio = null;
            string query = @"select
                        os.id_ordenservicio,
                        os.num_servicio,
                        cli.id_cliente,
                        cli.descripcion,
                        cli.descripcion,
                        os.tipo_servicio,
                        os.submarca,
                        os.precio,
                        os.fecha,
                        os.id_estatus,
                        est.descripcion estatus
                        from OrdenServicio os
                        inner join Cat_Cliente cli on os.id_cliente = cli.id_cliente
                        inner join Cat_Estatus est on os.id_estatus = est.id_estatus
                        where os.id_estatus = @estatus or 
                        os.fecha between @fechaInicio and @fechaFin";

            if (estatus != 0)
            {
                query += " order by estatus asc";
            }
            else if (fechaInicio != null && fechaFin != null) 
            {
                query += " order by fecha asc";
            }
           


            try
            {
                var connection = new BDConnection().getConnection();
                if (!connection.IsSuccess)
                {
                    return connection;
                }

                using (var sql = new SqlConnection(connection.Result.ToString()))
                {
                    using (var cmd = new SqlCommand(query, sql))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddWithValue("@estatus", estatus);

                        if (fechaInicio != null)   
                            cmd.Parameters.AddWithValue("@fechaInicio", fechaInicio);
                        else
                            cmd.Parameters.AddWithValue("@fechaInicio", DBNull.Value);

                        if (fechaFin != null)
                            cmd.Parameters.AddWithValue("@fechaFin", fechaFin);
                        else
                            cmd.Parameters.AddWithValue("@fechaFin", DBNull.Value);

                        sql.Open();
                        var rd = cmd.ExecuteReader();
                        if (rd.HasRows)
                        {
                            lstOrdenServicio = new List<OrdenServicio>();
                            while (rd.Read())
                            {
                                ordenServicio = new OrdenServicio();
                                ordenServicio.Servicio_ID = (int)rd["id_ordenservicio"];
                                ordenServicio.NumServicio = (int)rd["num_servicio"];
                                ordenServicio.Cliente.Cliente_ID = (int)rd["id_cliente"];
                                ordenServicio.Cliente.Nombre = (string)rd["descripcion"];
                                ordenServicio.Precio = (decimal)rd["precio"];
                                ordenServicio.Fecha = (DateTime)rd["fecha"];
                                ordenServicio.Estatus.Estatus_ID = (int)rd["id_estatus"];
                                ordenServicio.Estatus.Descripcion = (string)rd["estatus"];
                                ordenServicio.SubMarca = (string)rd["submarca"];
                                ordenServicio.TipoServicio = (string)rd["tipo_servicio"];
                                lstOrdenServicio.Add(ordenServicio);
                            }

                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se encontraron datos",
                                Result = lstOrdenServicio
                            };
                        }
                        else
                            throw new Exception("No se encontraron datos");
                    }
                }
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
 

        }
    }
}