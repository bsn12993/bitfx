﻿using BITFX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BITFX.Interface
{
    public interface IBase
    {
        /// <summary>
        /// Metodo a implementar, el cual realiza una consulta recuperando todos los registros existentes sin parametros o filtros
        /// </summary>
        /// <returns></returns>
        Response FindAll();
    }
}
