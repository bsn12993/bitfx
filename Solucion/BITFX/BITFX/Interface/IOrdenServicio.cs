﻿using BITFX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BITFX.Interface
{
    public interface IOrdenServicio : IBase
    {
        /// <summary>
        /// Metodo a implementar para recuperar registro con parametros especificos de la tabla
        /// </summary>
        /// <param name="estatus"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        Response FindByFilter(int estatus, DateTime? fechaInicio, DateTime? fechaFin);
    }
}