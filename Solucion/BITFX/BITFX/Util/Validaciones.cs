﻿using BITFX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BITFX.Util
{
    public class Validaciones
    {
        /// <summary>
        /// Metodo que compara las fechas, fecha inicio debe ser menor que la fecha fin
        /// </summary>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        public static Response ValidarFechas(DateTime? fechaInicio, DateTime? fechaFin)
        {
            if (fechaInicio < fechaFin) return new Response
            {
                IsSuccess = true,
                Message = "Fechas correctas"
            };
            return new Response
            {
                IsSuccess = false,
                Message = "Fechas incorrectas. La fecha Inicio debe ser menor a la fecha fin"
            };
        }
    }
}