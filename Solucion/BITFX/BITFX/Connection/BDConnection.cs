﻿using BITFX.Enum;
using BITFX.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BITFX.Connection
{
    public class BDConnection
    {
   
        /// <summary>
        /// Recupera el valor del ambiente del Web.Config
        /// </summary>
        private string Ambiente
        {
            get { return ConfigurationManager.AppSettings["Ambiente"].ToString(); }
        }
        /// <summary>
        /// Se utiliza para establecer la cadena de conexión en base al ambiente recuperado del web.config
        /// </summary>
        private string StringConnection { get; set; }
        /// <summary>
        /// Metodo que obtiene la cadena de conexión correspondiente al ambiente definido y recuperado en el web.config
        /// </summary>
        /// <returns></returns>
        public Response getConnection()
        {
            switch (this.Ambiente)
            {
                case "DEV":
                    StringConnection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    break;
                case "TEST":
                    StringConnection = ConfigurationManager.ConnectionStrings["TestConnection"].ConnectionString;
                    break;
                default:
                    return new Response
                    {
                        IsSuccess = false,
                        Message = "No se pudo obtener la cadena de conexión",
                        Result = ""
                    };
            }
            return new Response
            {
                IsSuccess = true,
                Message = "Se obtuvo la cadena de conexión del ambiente",
                Result = StringConnection
            };
        }
    }
}