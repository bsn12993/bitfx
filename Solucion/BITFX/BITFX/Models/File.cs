﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BITFX.Models
{
    public class File
    {
        private File()
        {

        }

        private static File instance;
        public string PDF { get; set; }
        public string EXCEL { get; set; }

        public static File GetInstance()
        {
            if (instance == null) return new File();
            else return instance;
        }

    }
}