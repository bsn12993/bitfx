﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BITFX.Models
{
    public class OrdenServicio
    {
        /// <summary>
        /// Identificador del servicio (id)
        /// </summary>
        public int Servicio_ID { get; set; }
        /// <summary>
        /// Número de servicio
        /// </summary>
        public int NumServicio { get; set; }
        /// <summary>
        /// Submarca
        /// </summary>
        public string SubMarca { get; set; }
        /// <summary>
        /// Cliente
        /// </summary>
        public Cliente Cliente { get; set; }
        /// <summary>
        /// Tipo Servicio
        /// </summary>
        public string TipoServicio { get; set; }
        /// <summary>
        /// Precio
        /// </summary>
        public decimal Precio { get; set; }
        /// <summary>
        /// Fecha
        /// </summary>
        public DateTime Fecha { get; set; }
        /// <summary>
        /// Estatus
        /// </summary>
        public Estatus Estatus { get; set; }

        public OrdenServicio()
        {
            Cliente = new Cliente();
            Estatus = new Estatus();
        }

    }
}