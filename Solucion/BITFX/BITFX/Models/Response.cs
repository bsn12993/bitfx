﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BITFX.Models
{
    public class Response
    {
        /// <summary>
        /// Bandera que indica si la operación se ejecuto correctamente
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// Mensaje de la respuesta
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Se almacena el objeto resultante de la operación (consultas de datos)
        /// </summary>
        public object Result { get; set; }
    }
}