﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BITFX.Models
{
    public class Cliente
    {
        /// <summary>
        /// ID del cliente
        /// </summary>
        public int Cliente_ID { get; set; }
        /// <summary>
        /// Nombre del cliente
        /// </summary>
        public string Nombre { get; set; }
    }
}