﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BITFX.Models
{
    public class Estatus
    {
        /// <summary>
        /// Id del estatus
        /// </summary>
        public int Estatus_ID { get; set; }
        /// <summary>
        /// nombre del estatus
        /// </summary>
        public string Descripcion { get; set; }
    }
}