﻿using BITFX.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace BITFX.Helpers
{
    public class FileHelper
    {

        /// <summary>
        /// Metodo estatico que genera un archivo pdf, este lo genera como base64, el cual se usa para mostrar en el navegador
        /// </summary>
        /// <param name="ordenServicios">lista de datos</param>
        /// <returns>archivo en base64</returns>
        public static Response GeneratePDF(List<OrdenServicio> ordenServicios)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                string base64 = string.Empty;

                string url = HttpContext.Current.Server.MapPath("~") + "Docs\\OrdenServicio" + DateTime.Now.ToShortDateString() + ".pdf";

                Document document = new Document(PageSize.LETTER);
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, memoryStream);
                document.Open();

                document.AddTitle("Orden de Servicio");

                document.Add(new Paragraph("Orden de Servicio"));
                document.Add(new Paragraph("Fecha generada: " + DateTime.Now.ToShortDateString()));
                document.Add(Chunk.NEWLINE);

                foreach (var i in ordenServicios)
                {
                    document.Add(new Paragraph($"Num. Servicio {i.NumServicio}"));
                    document.Add(new Paragraph($"Cliente {i.Cliente.Nombre}"));
                    document.Add(new Paragraph($"Submarca: {i.SubMarca}"));
                    document.Add(new Paragraph($"Tipo Servicio: {i.TipoServicio}"));
                    document.Add(new Paragraph($"Precio: {i.Precio}"));
                    document.Add(new Paragraph($"Fecha: {i.Fecha}"));
                    document.Add(new Paragraph($"Estatus: {i.Estatus.Descripcion}"));
                    document.Add(Chunk.NEWLINE);
                    document.Add(Chunk.NEWLINE);
                }

                document.Close();
                pdfWriter.Flush();
                pdfWriter.Close();
                var byteArray = memoryStream.ToArray();
                base64 = Convert.ToBase64String(byteArray);
                return new Response
                {
                    IsSuccess = true,
                    Message = "Se genero el archivo",
                    Result = base64
                };
            }
            catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = "Ocurrio un error al generar el archivo: " + e.Message
                };
            }
        }

        /// <summary>
        /// Metodo estatico que genera un archivo excel el cual retorna un valor base64, para mostrar en el navegador y descargarlo
        /// </summary>
        /// <param name="ordenServicios">lista de datos</param>
        /// <returns>base64</returns>
        public static Response GenerateExcel(List<OrdenServicio> ordenServicios)
        {
            try
            {
                IWorkbook workbook = new HSSFWorkbook();
                ISheet sheet1 = workbook.CreateSheet("Reporte");

                var style1 = workbook.CreateCellStyle();
                style1.FillForegroundColor = HSSFColor.Yellow.Index2;
                style1.FillPattern = FillPattern.SolidForeground;

                var fontDate = workbook.CreateFont();
                fontDate.FontHeightInPoints = 11;
                fontDate.FontName = "Calibri";
                fontDate.Boldweight = (short)FontBoldWeight.Bold;

                var fonttitle = workbook.CreateFont();
                fonttitle.FontHeightInPoints = 11;
                fonttitle.FontName = "Calibri";
                fonttitle.Boldweight = (short)FontBoldWeight.Bold;

                IRow rowDate = sheet1.CreateRow(0);
                rowDate.CreateCell(0).SetCellValue("Reporte Generado: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                rowDate.GetCell(0).CellStyle = workbook.CreateCellStyle();
                rowDate.GetCell(0).CellStyle.SetFont(fontDate);

                //Create Header 
                IRow rowTitle = sheet1.CreateRow(1);

                //Numero Servicio
                rowTitle.CreateCell(0).SetCellValue("Numero Servicio");
                rowTitle.GetCell(0).CellStyle = style1;
                rowTitle.GetCell(0).CellStyle = workbook.CreateCellStyle();
                rowTitle.GetCell(0).CellStyle.SetFont(fonttitle);

                //Cliente
                rowTitle.CreateCell(1).SetCellValue("Cliente");
                rowTitle.GetCell(1).CellStyle = style1;
                rowTitle.GetCell(1).CellStyle = workbook.CreateCellStyle();
                rowTitle.GetCell(1).CellStyle.SetFont(fonttitle);

                //Tipo Servicio
                rowTitle.CreateCell(2).SetCellValue("Tipo Servicio");
                rowTitle.GetCell(2).CellStyle = style1;
                rowTitle.GetCell(2).CellStyle = workbook.CreateCellStyle();
                rowTitle.GetCell(2).CellStyle.SetFont(fonttitle);

                //Submarca
                rowTitle.CreateCell(3).SetCellValue("Submarca");
                rowTitle.GetCell(3).CellStyle = style1;
                rowTitle.GetCell(3).CellStyle = workbook.CreateCellStyle();
                rowTitle.GetCell(3).CellStyle.SetFont(fonttitle);

                //Precio
                rowTitle.CreateCell(4).SetCellValue("Precio");
                rowTitle.GetCell(4).CellStyle = style1;
                rowTitle.GetCell(4).CellStyle = workbook.CreateCellStyle();
                rowTitle.GetCell(4).CellStyle.SetFont(fonttitle);

                //Fecha
                rowTitle.CreateCell(5).SetCellValue("Fecha");
                rowTitle.GetCell(5).CellStyle = style1;
                rowTitle.GetCell(5).CellStyle = workbook.CreateCellStyle();
                rowTitle.GetCell(5).CellStyle.SetFont(fonttitle);

                //Estatus
                rowTitle.CreateCell(6).SetCellValue("Estatus");
                rowTitle.GetCell(6).CellStyle = style1;
                rowTitle.GetCell(6).CellStyle = workbook.CreateCellStyle();
                rowTitle.GetCell(6).CellStyle.SetFont(fonttitle);

                

                //Write Data
                int newRow = 2;
                foreach (var data in ordenServicios)
                {
                    IRow row = sheet1.CreateRow(newRow);
                    row.CreateCell(0).SetCellValue(data.NumServicio);                 
                    row.CreateCell(1).SetCellValue(data.Cliente.Nombre);
                    row.CreateCell(2).SetCellValue(data.TipoServicio);
                    row.CreateCell(3).SetCellValue(data.SubMarca);
                    row.CreateCell(4).SetCellValue(data.Precio.ToString());
                    row.CreateCell(5).SetCellValue(data.Fecha);
                    row.CreateCell(6).SetCellValue(data.Estatus.Descripcion);   
                    newRow++;
                }

                sheet1.AutoSizeColumn(0);
                sheet1.AutoSizeColumn(1);
                sheet1.AutoSizeColumn(2);
                sheet1.AutoSizeColumn(3);
                sheet1.AutoSizeColumn(4);
                sheet1.AutoSizeColumn(5);
                sheet1.AutoSizeColumn(6);
                sheet1.AutoSizeColumn(7);
                sheet1.AutoSizeColumn(8);
                sheet1.AutoSizeColumn(9);

                MemoryStream memoryStream = new MemoryStream();

                //workbook.Write(fs);
                workbook.Write(memoryStream);
                var byteArray = memoryStream.ToArray();
                memoryStream.Flush();
                memoryStream.Close();
                var val = Convert.ToBase64String(byteArray);
                return new Response
                {
                    IsSuccess = true,
                    Message = "Se genero el archivo",
                    Result = val
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = "Ocurrio un error al generar el archivo: " + e.Message
                };
            }
        }

    }
}