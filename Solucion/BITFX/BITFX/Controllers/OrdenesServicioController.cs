﻿using BITFX.DAO;
using BITFX.Helpers;
using BITFX.Interface;
using BITFX.Models;
using BITFX.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BITFX.Controllers
{
    public class OrdenesServicioController : Controller
    {

        IOrdenServicio ServicioDAO;
        IBase EstatusDAO;

        // GET: OrdenesServicio
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Controlador principal que se muestra al entrar al proyecto
        /// Muestra el listado de registros
        /// </summary>
        /// <returns></returns>
        public ActionResult ListadoOrdenes()
        {
            return View();
        }

        /// <summary>
        /// Metodo que consulta y recupera los estatus existentes para mostrarlos en un Select
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetEstatus()
        {
            JsonResult json = new JsonResult();
            EstatusDAO = new EstatusDAO();
            var estatus = EstatusDAO.FindAll();
            if (estatus.IsSuccess)
            {
                var jObject = JsonConvert.SerializeObject(estatus.Result);
                json.Data = new { IsSuccess = estatus.IsSuccess, Message = estatus.Message, Result = jObject };
            }
            else
                json.Data = new { IsSuccess = estatus.IsSuccess, Message = estatus.Message, Result = "" };
            return json;
        }

        /// <summary>
        /// Metodo que obtiene todos los registros de la tabla existentes
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetListadoOrdenServicio()
        {
            JsonResult json = new JsonResult();
            ServicioDAO = new OrdenServicioDAO();
            var datos = ServicioDAO.FindAll();
            if (datos.IsSuccess)
            {
                var jObject = JsonConvert.SerializeObject(datos.Result);
                json.Data = new { IsSuccess = datos.IsSuccess, Message = datos.Message, Result = jObject };
            }
            else
                json.Data = new { IsSuccess = datos.IsSuccess, Message = datos.Message, Result = "" };
            return json;
        }

        /// <summary>
        /// Controlador que muestra los resultados de la consulta en base a los filtros
        /// </summary>
        /// <param name="estatus"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Filtrar(int estatus, DateTime? fechaInicio, DateTime? fechaFin)
        {
            JsonResult json = new JsonResult();
            ServicioDAO = new OrdenServicioDAO();

            if (estatus != 0 || (fechaInicio.HasValue && fechaFin.HasValue)) 
            {
                if (fechaInicio != null && fechaFin != null)
                {
                    var validarFechas = Validaciones.ValidarFechas(fechaInicio, fechaFin);
                    if (!validarFechas.IsSuccess)
                    {
                        json.Data = new { IsSuccess = validarFechas.IsSuccess, Message = validarFechas.Message, Result = "" };
                        return json;
                    }
                }
                else if (estatus == 0)
                {
                    json.Data = new { IsSuccess = false, Message = "Seleccione un estatus correcto", Result = "" };
                    return json;
                }                
            }
            else
            {
                json.Data = new { IsSuccess = false, Message = "Seleccione un estatus o rango de fechas para buscar", Result = "" };
                return json;
            }
             
 

            var datos = ServicioDAO.FindByFilter(estatus, fechaInicio, fechaFin);
            if (datos.IsSuccess)
            {
                var jObject = JsonConvert.SerializeObject(datos.Result);
                json.Data = new { IsSuccess = datos.IsSuccess, Message = datos.Message, Result = jObject };
            }
            else
                json.Data = new { IsSuccess = datos.IsSuccess, Message = datos.Message, Result = "" };
            return json;
        }


        /// <summary>
        /// Metodo que genera el archivo PDF
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CreatePDF(int estatus, DateTime? fechaInicio, DateTime? fechaFin)
        {
            JsonResult json = new JsonResult();
            ServicioDAO = new OrdenServicioDAO();

            if (estatus != 0 || (fechaInicio.HasValue && fechaFin.HasValue))
            {
                if (fechaInicio != null && fechaFin != null)
                {
                    var validarFechas = Validaciones.ValidarFechas(fechaInicio, fechaFin);
                    if (!validarFechas.IsSuccess)
                    {
                        json.Data = new { IsSuccess = validarFechas.IsSuccess, Message = validarFechas.Message, Result = "" };
                        return json;
                    }
                }
                else if (estatus == 0)
                {
                    json.Data = new { IsSuccess = false, Message = "Seleccione un estatus correcto", Result = "" };
                    return json;
                }
            }
            else
            {
                json.Data = new { IsSuccess = false, Message = "Seleccione un estatus o rango de fechas para buscar", Result = "" };
                return json;
            }


            var datos = ServicioDAO.FindByFilter(estatus, fechaInicio, fechaFin);
            if (datos.IsSuccess)
            {
                var file = FileHelper.GeneratePDF((List<OrdenServicio>)datos.Result);
                if (file.IsSuccess)
                    json.Data = new { IsSuccess = true, data = file.Result };
                else
                    json.Data = new { IsSuccess = false, Message = file.Message };
            }
            else
            {
                json.Data = new { IsSuccess = false, Message = "Ocurrio un problema al generar el archivo" };
            }
            return json;
        }

        /// <summary>
        /// Metodo que genera un archivo EXCEL
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CreateEXCEL(int estatus, DateTime? fechaInicio, DateTime? fechaFin)
        {
            JsonResult json = new JsonResult();
            ServicioDAO = new OrdenServicioDAO();

            if (estatus != 0 || (fechaInicio.HasValue && fechaFin.HasValue))
            {
                if (fechaInicio != null && fechaFin != null)
                {
                    var validarFechas = Validaciones.ValidarFechas(fechaInicio, fechaFin);
                    if (!validarFechas.IsSuccess)
                    {
                        json.Data = new { IsSuccess = validarFechas.IsSuccess, Message = validarFechas.Message, Result = "" };
                        return json;
                    }
                }
                else if (estatus == 0)
                {
                    json.Data = new { IsSuccess = false, Message = "Seleccione un estatus correcto", Result = "" };
                    return json;
                }
            }
            else
            {
                json.Data = new { IsSuccess = false, Message = "Seleccione un estatus o rango de fechas para buscar", Result = "" };
                return json;
            }


            var datos = ServicioDAO.FindByFilter(estatus, fechaInicio, fechaFin);
            if (datos.IsSuccess)
            {
                var file = FileHelper.GenerateExcel((List<OrdenServicio>)datos.Result);
                if(file.IsSuccess)
                    json.Data = new { IsSuccess = true, data = file.Result };
                else
                    json.Data = new { IsSuccess = false, Message = file.Message };
            }
            else
            {
                json.Data = new { IsSuccess = false, Message = "Ocurrio un problema al generar el archivo" };
            }
            return json;
        }

    }
}