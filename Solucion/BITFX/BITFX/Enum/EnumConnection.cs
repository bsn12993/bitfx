﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BITFX.Enum
{
    public enum EnumConnection
    {
        /// <summary>
        /// Ambiente desarrollo o local
        /// </summary>
        DEV=1,
        /// <summary>
        /// Ambiente pruebas
        /// </summary>
        TEST=2
    }
}