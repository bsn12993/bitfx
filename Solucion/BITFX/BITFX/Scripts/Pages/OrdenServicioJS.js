﻿//función que inicia al cargar la página
$(function () {
    $("#msg").hide();
    GetEstatus();
    GetListado();
});

//objecto json para enviar datos 
var json = {
    estatus: 0,
    fechaInicio: null,
    fechaFin: null
};

//evento del boton para generar PDF
$("#btn_PDF").click(function () {
    GetReport("/OrdenesServicio/CreatePDF","data:application/pdf;base64,");
});

//evento del boton para generar EXCEL
$("#btn_EXCEL").click(function () {
    GetReport("/OrdenesServicio/CreateEXCEL", "data:application/vnd.ms-excel;base64,");
});

//función para crear archivo PDF o EXCEL, dependiendo del evento del boton y del valor de type
function GetReport(url, type) {
    $("#txt_fechaInicio").css("border", "");
    $("#txt_fechaFin").css("border", "");
    $("#cmb_estatus").css("border", "");
    var estatus = $("#cmb_estatus").val();
    var fechaInicio = $("#txt_fechaInicio").val();
    var fechaFin = $("#txt_fechaFin").val();


    if (estatus != 0 || (fechaInicio != "" && fechaFin != "")) {
        if (fechaInicio != "" && fechaFin != "") {

            var FI = fechaInicio.split("-");
            var dFInicio = new Date(FI[0], FI[1] - 1, FI[2]);

            if (fechaFin == "") {
                $("#div_modal").modal("show");
                $("#title").html("Advertencia");
                $("#text").html("Debe seleccionar una fecha fin");
                $("#txt_fechaFin").css("border", "2px solid red");
                return;
            }

            var FF = fechaFin.split("-");
            var dFFin = new Date(FF[0], FF[1] - 1, FF[2]);

            if (dFInicio.valueOf() > dFFin.valueOf()) {
                $("#div_modal").modal("show");
                $("#title").html("Advertencia");
                $("#text").html("La fecha Inicio no puede ser mayor a la fecha fin");
                $("#txt_fechaInicio").css("border", "2px solid red");
                $("#txt_fechaFin").css("border", "2px solid red");
                return;
            }
        }
        else if (estatus == 0) {
            $("#div_modal").modal("show");
            $("#title").html("Advertencia");
            $("#text").html("Seleccione un estatus");
            $("#cmb_estatus").css("border", "2px solid red");
            return;
        }
    }
    else if (fechaFin != "" && fechaInicio == "") {
        $("#div_modal").modal("show");
        $("#title").html("Advertencia");
        $("#text").html("Debe seleccionar una fecha de inicio");
        $("#txt_fechaInicio").css("border", "2px solid red");
        return;
    }
    else if (fechaInicio != "" && fechaFin == "") {
        $("#div_modal").modal("show");
        $("#title").html("Advertencia");
        $("#text").html("Debe seleccionar una fecha fin");
        $("#txt_fechaFin").css("border", "2px solid red");
        return;
    }
    else {
        $("#div_modal").modal("show");
        $("#title").html("Advertencia");
        $("#text").html("Seleccione un estatus o rango de fechas para buscar");
        $("#txt_fechaInicio").css("border", "2px solid red");
        $("#txt_fechaFin").css("border", "2px solid red");
        $("#cmb_estatus").css("border", "2px solid red");
        return;
    }


    json.estatus = estatus;
    json.fechaInicio = fechaInicio;
    json.fechaFin = fechaFin;

    $.ajax({
        url: url,
        cache: false,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(json),
        beforeSend: function () {
            $("#msg").show();
            $("#msg").html("Generando Archivo...");
        },
        success: function (response) {
            console.log(response);
            $("#msg").html("");
            $("#msg").hide();
            if (response.IsSuccess) {
                window.open(type + response.data, "_target");
            }
            else {
                alert(response.Message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert(xhr.responseText);
        },
        complete: function () {

        }
    });
}


//evento del formulario para realizar la búsqueda de registros
$("#form_busqueda").on('submit', function (evt) {
    evt.preventDefault();
    $("#txt_fechaInicio").css("border", "");
    $("#txt_fechaFin").css("border", "");
    $("#cmb_estatus").css("border", "");
    var form = $(this);
    var url = form.attr("action");
 

    var estatus = $("#cmb_estatus").val();
    var fechaInicio = $("#txt_fechaInicio").val();
    var fechaFin = $("#txt_fechaFin").val();

    if (estatus != 0 || (fechaInicio != "" && fechaFin != "")) {
        if (fechaInicio != "" && fechaFin != "") {

            var FI = fechaInicio.split("-");
            var dFInicio = new Date(FI[0], FI[1] - 1, FI[2]);

            if (fechaFin == "") {
                $("#div_modal").modal("show");
                $("#title").html("Advertencia");
                $("#text").html("Debe seleccionar una fecha fin");
                $("#txt_fechaFin").css("border", "2px solid red");
                return;
            }

            var FF = fechaFin.split("-");
            var dFFin = new Date(FF[0], FF[1] - 1, FF[2]);

            if (dFInicio.valueOf() > dFFin.valueOf()) {
                $("#div_modal").modal("show");
                $("#title").html("Advertencia");
                $("#text").html("La fecha Inicio no puede ser mayor a la fecha fin");
                $("#txt_fechaInicio").css("border", "2px solid red");
                $("#txt_fechaFin").css("border", "2px solid red");
                return;
            }
        }
        else if (estatus == 0) {
            $("#div_modal").modal("show");
            $("#title").html("Advertencia");
            $("#text").html("Seleccione un estatus");
            $("#cmb_estatus").css("border", "2px solid red");
            return;
        }
    }
    else if (fechaFin != "" && fechaInicio == "") {
        $("#div_modal").modal("show");
        $("#title").html("Advertencia");
        $("#text").html("Debe seleccionar una fecha de inicio");
        $("#txt_fechaInicio").css("border", "2px solid red");
        return;
    }
    else if (fechaInicio != "" && fechaFin == "") {
        $("#div_modal").modal("show");
        $("#title").html("Advertencia");
        $("#text").html("Debe seleccionar una fecha fin");
        $("#txt_fechaFin").css("border", "2px solid red");
        return;
    }
    else {
        $("#div_modal").modal("show");
        $("#title").html("Advertencia");
        $("#text").html("Seleccione un estatus o rango de fechas para buscar");
        $("#txt_fechaInicio").css("border", "2px solid red");
        $("#txt_fechaFin").css("border", "2px solid red");
        $("#cmb_estatus").css("border", "2px solid red");
        return;
    }

     

    json.estatus = estatus;
    json.fechaInicio = fechaInicio;
    json.fechaFin = fechaFin;


    $.ajax({
        url: url,
        cache: false,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(json),
        beforeSend: function () {
            $("#msg").show();
            $("#msg").html("Buscando datos...");
        },
        success: function (response) {
            console.log(response);
            $("#msg").html("");
            $("#msg").hide();
            if (response.IsSuccess) {
                var json = JSON.parse(response.Result);
                console.log(json);
                $(document).ready(function () {
                    $('#tbl_ordenesServicio').DataTable().destroy();
                    $('#tbl_ordenesServicio').DataTable({
                        data: json,
                        columns: [
                            { data: "NumServicio" },
                            { data: "Cliente.Nombre" },
                            { data: "TipoServicio" },
                            { data: "Precio" },
                            { data: "SubMarca" },
                            {
                                data: "Fecha", render: function (data) {
                                    var d = data.split("T");
                                    return d;
                                }
                            },
                            { data: "Estatus.Descripcion" }
                        ],
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                        },
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                });
            }
            else {
                alert(response.Message);
                $('#tbl_ordenesServicio').DataTable().clear().draw();
                $('#tbl_ordenesServicio').DataTable();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert(xhr.responseText);
        },
        complete: function () {
            $("#msg").hide();
            $("#msg").html("");
        }
    });
});


//función que consulta y muestra todos los datos existentes 
function GetListado() {
    $.ajax({
        url: "/OrdenesServicio/GetListadoOrdenServicio",
        cache: false,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: {},
        beforeSend: function () {
            $("#msg").show();
            $("#msg").html("Cargando Datos");
        },
        success: function (response) {
            console.log(response);
            $("#msg").html("");
            $("#msg").hide();
            if (response.IsSuccess) {
                var json = JSON.parse(response.Result);
                console.log(json);
                $(document).ready(function () {
                    $('#tbl_ordenesServicio').DataTable({
                        data: json,
                        columns: [
                            { data: "NumServicio" },
                            { data: "Cliente.Nombre" },
                            { data: "TipoServicio" },
                            { data: "Precio" },
                            { data: "SubMarca" },
                            {
                                data: "Fecha", render: function (data) {
                                    var d = data.split("T");
                                    return d;
                                }
                            },
                            { data: "Estatus.Descripcion" }
                        ],
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                        },
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                });
            }
            else {
                alert(response.Message);
                $('#tbl_ordenesServicio').DataTable();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert(xhr.responseText);
        },
        complete: function () {
            $("#msg").hide();
            $("#msg").html("");
        }
    });
}

//función que obtiene los estatus existentes cargandolos en un select
function GetEstatus() {
    $.ajax({
        url: "/OrdenesServicio/GetEstatus",
        cache: false,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: {},
        beforeSend: function () {
             
        },
        success: function (response) {
            console.log(response);
            if (response.IsSuccess) {
                var json = JSON.parse(response.Result);
                console.log(json);
                $.each(json, function (k, v) {
                    $("#cmb_estatus").append('<option value=' + v.Estatus_ID + '>' + v.Descripcion + '</option>');
                });
            }
            else {
                alert(response.Message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert(xhr.responseText);
        },
        complete: function () {

        }
    });
}
 