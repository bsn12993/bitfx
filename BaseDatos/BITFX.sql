create database BITFX;
use BITFX;

/** Tabla catalogo para los clientes**/
create table Cat_Cliente(
id_cliente int identity primary key,
descripcion varchar(100)
);

/** Tabla catalogo para los estatus**/
create table Cat_Estatus(
id_estatus int identity primary key,
descripcion varchar(100)
);

/** Tabla orden de servicio**/
create table OrdenServicio(
id_ordenservicio int identity primary key,
num_servicio int,
id_cliente int,
tipo_servicio varchar(100),
submarca varchar(100),
precio decimal,
fecha datetime,
id_estatus int,
FOREIGN KEY (id_cliente) REFERENCES Cat_Cliente(id_cliente),
FOREIGN KEY (id_estatus) REFERENCES Cat_Estatus(id_estatus)
)





 